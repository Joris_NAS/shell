#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int main(){

// declaration de nos variables
 int nombre_mystere = 0, nombre_utilisateur = 0, tentative =1;
 const int VALEUR_MIN = 1, VALEUR_MAX = 100;
 
 // generation d'un nombre aleatoire
 srand(time(NULL));
 nombre_mystere = (rand() % (VALEUR_MAX - VALEUR_MIN + 1)) + VALEUR_MIN;
 printf("Devinez quel est mon nombre mystere. Indice: c'est un nombre entre %d et %d \n", VALEUR_MIN, VALEUR_MAX);
 scanf ("%d",&nombre_utilisateur);
 	
	while ( nombre_utilisateur != nombre_mystere) {
		if ( nombre_utilisateur < nombre_mystere){
			printf("tentative %d -> %d \n", tentative , nombre_utilisateur);
			printf("C'est plus!\n");
			tentative = tentative +1 ;
			}
		else {
			printf("tentative %d -> %d \n", tentative , nombre_utilisateur);
			printf("C'est moin!\n");
			tentative = tentative +1 ;
			}
		printf("Essayer à nouveau. Indice: c'est un nombre entre %d et %d \n", VALEUR_MIN, VALEUR_MAX);
 		scanf ("%d",&nombre_utilisateur);

	}
 printf("Bravo! le nombre mystère est bien %d\n", nombre_mystere);	
 return 0;
}
