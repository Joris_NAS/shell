#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void ecriture_fichier()
{
	 FILE *fichier; /* pointeur sur FILE */
     char matricule[256], nom[256], prenom[256];
     int  i,nb_save;

	/* Première partie : 
     Créer et remplir le fichier */
 	 fichier = fopen("inform1.txt", "w");  /* write */
 	 printf("Nombre d'enregistrements à créer : ");
 	 scanf("%d", &nb_save);
  	 i = 0;
	 fseek (fichier,4,SEEK_END);
  	 while (i<nb_save)
     {
      printf("\nMatricule:\n");
      scanf("%s", matricule);
      printf("\nNom:\n");
      scanf("%s", nom);
      printf("\nPrenom:\n");
      scanf("%s", prenom);
      fprintf(fichier, "Matricule : %s\n nom : %s\n prenom : %s\n", matricule, nom, prenom);
      i++;
     }
     rename("inform1.txt","inform2.txt");
     fclose(fichier);
}

void afficher_fichier()
{
	  /* Deuxième partie : 
     Lire et afficher le contenu du fichier */
	FILE *fc; /* pointeur sur FILE */
  	fc = fopen("inform2.TXT", "r");  /* read */
  	printf("\n------------------------ Affichage du fichier INFORM.TXT renommer inform2.txt ---------------------------\n");
	signed char s[256];
	while(fgets(s,255,fc) != NULL)
	{
		printf("%s",s);
	}
  fclose(fc);
}

int main()
{
     ecriture_fichier();
     afficher_fichier();
	 return 0;
}

