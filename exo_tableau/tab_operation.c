#include <stdio.h>
#include <stdlib.h>
//#include "tableau.h"

//#define taille_tab 15

int somme_tableau(int *tab, int taille_tab){

 int somme = 0;
 //calcul de somme des elements du tableau

 for ( int i =0 ; i< taille_tab ; i++){
                somme = somme + tab[i];
        }
 return somme ;
}

int max_tableau( int *tab, int taille_tab){

 int max =0;
 //calcul de la valeur max du tableau
 for ( int i =1 ; i< taille_tab ; i++){
                max = tab[0];
                if (tab[i]>max ){
                max = tab[i];
                }
        }
 return max ;

 }

int min_tableau( int *tab, int taille_tab){

 int min =0;
 //calcul de la valeur min du tableau
 for ( int i =1 ; i < taille_tab ; i++){
                min = tab [0];
                if (tab[i]< min ){
                min = tab[i];
                }
        }
 return min ;
 }

int recherche_valeur( int *tab, int taille_tab, int val){

 int min =0;
 int k = 0;
 //RECHERCHE
 for ( int i =0 ; i<taille_tab ; i++){
                min = tab[i];
                if (val == tab[i]){
                min = 1;
                k++;
                }
        }
 printf("%d - la valeur %d est presente %d fois dans le tableau\n", min, val, k);

 }

int main(){

 int tab [15] = {0,45,5,56,8,2,0,3,9,35,3,7,22,-87,19};
 int val,choix,a,b,c ;
 
 //menu de choix 
printf("1-Calculer la somme du tableau predefinis \n ou\n2-tester la présence d’une valeur dans un tableau\n3- Connaitre le min et le max de notre tableau\n ");
scanf ("%d",&choix);

if ( choix == 1) {
		printf("la somme du tableau est %d\n",somme_tableau(tab,15));
	}
else if (choix == 2){
		printf("Entrer la valeur recherché\n");
		scanf ("%d",&val);
		recherche_valeur(tab,15,val);
		
	}
else if (choix == 3){

		printf("le min est %d et le max est %d\n",min_tableau(tab,15),max_tableau(tab,15));

	} 
 return 0 ;
}
