#include <stdio.h>
#include <stdlib.h>

int main(){
	int a = 2; 
	int taille_a = sizeof(int);
	int taille_b = sizeof(long);
        int taille_c = sizeof(short);
	int taille_d = sizeof(float);
	int taille_e = sizeof(double);
	int taille_f = sizeof(char);
	long b = 3777; 
	short c = 4; 
	float d = 5.5;
	double e = 7.76767;
	char f = 'y';

	printf("TP2: Memoire et Variables”.\n");
	printf("Mon int est : %d de taille %d\n",a, taille_a);
	printf("Mon long est :%ld de taille %d\n", b, taille_b);
	printf("Mon short est :%d  de taille %d\n",c, taille_c);
	printf("Mon float est :%f de taille %d\n",d, taille_d);
	printf("Mon double est :%d de taille %d\n",e, taille_e);
	printf("Mon char est :%c de taille %d\n",f, taille_f);
	return 0;
	}
