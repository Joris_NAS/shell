#include <stdio.h>
#include <stdlib.h>


int main(){
	int nb1;
	int nb2;
	char operateur;
	int resultat;
	
	printf("Indiquez l'operation mathematique que je doit resoudre (sans espace) \n");
	printf("-sous la forme [nombre1 opérateur nombre2] par exemple (2+3)\n");
	printf("-operateurs possibles [+, -, *, /]\n");
	scanf ("%d%c%d",&nb1 ,&operateur ,&nb2);
	switch ( operateur){
		case '+':
			resultat = nb1 + nb2 ;
			printf(" le résultat de %d %c %d est: %d \n", nb1, operateur, nb2, resultat);
		break;
		case '-':
			resultat = nb1 - nb2 ;
			printf(" le résultat de %d %c %d est: %d \n", nb1, operateur, nb2, resultat);
		break;
		case '*':
			resultat = nb1 * nb2 ;
			printf(" le résultat de %d %c %d est: %d \n", nb1, operateur, nb2, resultat);
		break;
		case '/':
			if ( nb2 ==0){
				printf("Attention la division par 0 est impossible!\n");			}
			else {resultat = nb1 / nb2 ;
				printf(" le résultat de %d %c %d est: %d \n", nb1, operateur, nb2, resultat);
			}
		break;
		case '%':
			if ( nb2 ==0){
				printf("Attention la division par 0 est impossible! \n");			}
			else {
			resultat = nb1 % nb2 ;
			printf(" le résultat de %d %c %d est: %d \n", nb1, operateur, nb2, resultat);
			}
		break;
		default:
			printf("Opérateur invalide, vous devez utilisez l'un des opérateur (+, -, *, /)");
		break;
	}
	return 0;
}
